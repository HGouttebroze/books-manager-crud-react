import React from 'react';
import { NavLink } from 'react-router-dom';

const Header = () => {
  return (
    <header>
      <h1>Bienvenue sur ma librairie!</h1> 
      <hr />
      <div className="links">
        <NavLink to="/" className="link" activeClassName="active" exact>
          Livres
        </NavLink>
        <NavLink to="/add" className="link" activeClassName="active">
          Ajouter
        </NavLink>
      </div>
    </header>
  );
};

export default Header;